import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    groceries: []
  },
  mutations: {
    addGrocery (state, element) {
      state.groceries.push(element)
    }
  },
  actions: {
  },
  modules: {
  }
})
