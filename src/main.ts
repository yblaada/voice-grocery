import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import VModal from 'vue-js-modal'
import store from './store'
import json from './static/database.json'
const database = json.fruits.concat(json.veggies)
Vue.mixin({
  methods: {
    process: function (speech) {
      const result = []
      database.forEach(element => {
        console.log(speech.toLowerCase())
        if(element.name.toLowerCase().includes(speech.toLowerCase())) {
          result.push(element)
        }
      });
      return result
    },
  },
})

Vue.config.productionTip = false

Vue.prototype.$companyName = "Marché Express"
Vue.prototype.$leftmsg = "left-msg"
Vue.prototype.$rightmsg = "right-msg"
Vue.use(VModal)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
